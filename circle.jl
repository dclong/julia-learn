type Circle
    radius::Float64
    function area(r::Float64)
        return pi * r * r
    end
    function area()
        return area(radius) 
    end
    Circle(r::Float64) = new(r)
end
