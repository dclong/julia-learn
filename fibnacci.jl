function fib(n)
        x1 = 1
        x2 = 0
        for i = 0:n
                temp = x1 + x2
                x1 = x2
                x2 = temp
        end
        return x2
end
